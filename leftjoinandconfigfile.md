# left join x config file

## cfg.ini
```
[INPUT]
filename1 = FIRSTNAMES.csv
filename2 = LASTNAMES.csv

[COLNAMES]
left=IDS
right=IDS
drop=HEIGHT,AGE
```


## main.py
```
import pandas 
import configparser

config = configparser.ConfigParser()
assert(config.read('cfg.ini') != [] )

file1 = config['INPUT']['filename1']
file2 = config['INPUT']['filename2']

join_on_file1 = config['COLNAMES']['left']
join_on_file2 = config['COLNAMES']['right']

df1 = pandas.read_csv(file1, sep=';')
df2 = pandas.read_csv(file2, sep=';')

result = pandas.merge(df1, df2, left_on=join_on_file1, right_on=join_on_file2, how='left').drop_duplicates()

columns_to_drop = config['COLNAMES']['drop'].split(',')

for i in list(result.columns) : 
    if i in columns_to_drop : 
        result.drop(columns=[i], inplace=True)

result.to_csv('out.csv', index=False)
```
