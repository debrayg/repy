## drop duplicates
import pandas

data = {
        'NAMES' : ['Jack', 'Paul', 'Dean', 'Jack'],
        'JOBS' : ['plumber', 'artist', 'poet', 'salesman']
}

df = pandas.DataFrame(data).drop_duplicates(subset=['NAMES'], keep='first')
#   NAMES     JOBS
# 0  Jack  plumber
# 1  Paul   artist
# 2  Dean     poet


df2 = pandas.DataFrame(data).drop_duplicates(subset=['NAMES'], keep='last')
#  NAMES      JOBS
# 1  Paul    artist
# 2  Dean      poet
# 3  Jack  salesman
