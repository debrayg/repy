# if (condition in column 'A') : df['A'] = df['B'] 

import pandas as pd

data = {
        'IDS' : [0, 1, 2, 3],
        'NAMES' : ['Ali', 'Dean', 'Paul', 'Jack'],
        'AGES' : [18, 22, None, 33]
}

df1 = pd.DataFrame(data)


assert(df1.at[2, 'NAMES'] == 'Paul')

# change value in 1 column to value of another column if condition is True
if df1.at[2, 'NAMES'] == 'Paul' : 
    df1.at[2, 'NAMES'] = df1.at[3, 'NAMES']

assert(df1.at[2, 'NAMES'] == 'Jack')
