## left join duplicate example

import pandas as pandas

data1 = {
        'IDS' : [0, 1, 2, 3],
        'NAMES' : ['Ali', 'Dean', 'Paul', 'Jack'],
        'AGES' : [18, 22, None, 33]
}

df1 = pandas.DataFrame(data1)
df1['AGES'] = df1['AGES'].fillna(0)
df1['AGES'] = df1['AGES'].astype(int)

#    IDS NAMES  AGES
# 0    0   Ali    18
# 1    1  Dean    22
# 2    2  Paul     0
# 3    3  Jack    33

data2 = {
        'ID' : [3, 1, 2, 3],
        'JOBS' : ['plumber', 'artist', 'poet', 'salesman']
}
df2 = pandas.DataFrame(data2)

#    ID      JOBS
# 0   3   plumber <-
# 1   1    artist
# 2   2      poet
# 3   3  salesman <-

# duplicate on ID 3


result = pandas.merge(df1, df2, left_on='IDS', right_on="ID", how="left")
result.drop(columns=['ID'], inplace=True)
print(result)

#    IDS NAMES  AGES      JOBS
# 0    0   Ali    18       NaN
# 1    1  Dean    22    artist
# 2    2  Paul     0      poet
# 3    3  Jack    33   plumber <-
# 4    3  Jack    33  salesman <-

# duplicate on final left join because duplicate in df2['ID']
