# change column value based on condition

import pandas

data1 = {
        'IDS' : [0, 1, 2, 3],
        'NAMES' : ['Ali', 'Dean', 'Paul', 'Jack'],
        'AGES' : [18, 22, 19, 33],
        'NAMES_MAJ' : ['ALI', 'DEAN', 'PAUL', 'JACK']
}
df = pandas.DataFrame(data1)

df.loc[df['NAMES']=='Dean', 'NAMES'] = 'DEAN'

# change value of column 'NAMES' to value of column 'NAMES_MAJ' if condition fulfilled
df.loc[df['NAMES']=='Paul', 'NAMES'] = df['NAMES_MAJ']

"""
   IDS NAMES  AGES NAMES_MAJ
0    0   Ali    18       ALI
1    1  DEAN    22      DEAN
2    2  PAUL    19      PAUL
3    3  Jack    33      JACK
"""