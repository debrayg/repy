#!/usr/bin/env python3
import pandas as pd

df_norsemen = pd.read_csv("NORSEMEN.csv", dtype={"ID" : str})
df_norsegods= pd.read_csv("NORSEGODS.csv", dtype={"IDS" : str})

df_norsegods['AGE'] = None
df_norsegods.rename(columns={'IDS' : 'ID'}, inplace=True)

df_norsemen['MISC'] = None
df_norsemen = df_norsemen[['ID', 'NAME', 'MISC', 'AGE']]


df_merged = pd.concat([df_norsemen, df_norsegods], ignore_index=True)
df_merged.to_csv("merged.csv", index=False)


# get names
names = list(df_merged["NAME"])
assert(type(names) == list)

namestoo = df_merged.iloc[:,0]
assert(type(namestoo) == pd.core.series.Series)
# TODO : how to manipulate Series in pandas




# at syntax
assert(df_merged.at[1, "NAME"] == 'Knut')



# keep rows with specific conditions
df2 = df_merged.loc[df_merged['AGE'] > 22]

# keep only rows with empty "AGE"
df3 = df_merged.loc[df_merged['AGE'].isna()]
# same as : df3 = df_merged.loc[df_merged['AGE'].isnull()]

# get 3 first rows
df4 = df_merged.iloc[:3]

# make new df with only columns ID and NAME
df5 = df_merged[['ID', 'NAME']]







# get columns on dfA but not on dfB : 
dfA = pd.read_csv("NORSEMEN.csv", sep=',')
dfB = pd.read_csv("NORSEMEN2.csv", sep=',')

merged = dfA.merge(dfB, how='left', indicator=True)
only_in_dfA = merged[merged['_merge']=='left_only'].drop('_merge', axis=1)




# TODO : left join
