#!/bin/bash

sudo apt install python3-venv
mkdir workdir
cd workdir
python3 -m venv .
source bin/activate
which pip3
pip3 install pandas
pip3 install openpyxl

echo "import pandas
data = {
        'NAMES' : ['Ali', 'Dean', 'Paul', 'Jack'],
        'AGES' : [18, 22, 21, 33]
}
people = pandas.DataFrame(data)
print(people)
" > script.py

python3 script.py