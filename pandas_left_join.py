# left join demo python

import pandas

data1 = {
        'IDS' : [0, 1, 2, 3],
        'NAMES' : ['Ali', 'Dean', 'Paul', 'Jack'],
        'AGES' : [18, 22, None, 33]
}
df1 = pandas.DataFrame(data1)


data2 = {
        'ID' : [3, 1, 2],
        'JOBS' : ['plumber', 'artist', 'poet']
}
df2 = pandas.DataFrame(data2)


result = pandas.merge(df1, df2, left_on='IDS', right_on="ID", how="left").drop_duplicates()
result.drop(columns=['ID'], inplace=True) # or result = result.drop('ID', axis=1)

print(result)